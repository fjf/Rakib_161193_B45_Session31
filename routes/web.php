<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/goodbye', function () {
    return view('goodbye');
});



Route::get('/students/index', 'StudentsController@index' );


Route::get('/students/create', function () {
    return view('Students.create');
});


Route::post('/students/store', 'StudentsController@store' );


Route::get('/students/view/{id}', array("uses"=>'StudentsController@view') );

















